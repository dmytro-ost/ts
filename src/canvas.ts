import {
  Point,
  Shape,
  Line,
  Triangle,
  Square,
  Circle,
  Polygon,
} from "./shapes.js";

export class Canvas {
  tool: Shape = new Line();
  canvas: HTMLCanvasElement;
  rect: DOMRect;
  ctx: CanvasRenderingContext2D;

  constructor(root: string, width: string = "800", height: string = "600") {
    this.canvas = document.createElement("canvas");
    this.canvas.id = "field";
    this.canvas.setAttribute("width", width);
    this.canvas.setAttribute("height", height);

    let container: HTMLElement = document.querySelector(root);

    container.innerHTML = `<div class="choice-control">
    <label>
      <input type="radio" name="radio" value="Line" checked="checked">
      Line
    </label>
    <label>
      <input type="radio" name="radio" value="Triangle">
      Triangle
    </label>
    <label>
      <input type="radio" name="radio" value="Square">
      Square
    </label>
    <label>
      <input type="radio" name="radio" value="Circle">
      Circle
    </label>
    <label>
      <input type="radio" name="radio" value="Polygon">
      Polygon with <input id="vertex" type="number" min="3" max="100"> points.
    </label>
    <p>Choose a shape and mark points on the canvas</p>
    <p class="result"></p>
  </div>`;

    document.querySelector(root).appendChild(this.canvas);

    this.rect = this.canvas.getBoundingClientRect();
    this.ctx = this.canvas.getContext("2d");
  }

  setEventListener(): void {
    this.canvas.addEventListener("click", (e: MouseEvent) => {
      let strOut: string = this.tool.setPointAndDraw(
        new Point(e.clientX - this.rect.left, e.clientY - this.rect.top),
        this.ctx,
        +(document.getElementById("vertex") as HTMLInputElement).value
      );

      document.querySelector(".result").innerHTML = strOut;
    });

    let radioBtn: NodeListOf<HTMLElement> = document.querySelectorAll(
      "input[type=radio][name=radio]"
    );

    radioBtn.forEach((btn) => {
      btn.addEventListener("change", (e: MouseEvent) => {
        let rb: HTMLInputElement = <HTMLInputElement>e.target;

        switch (rb.value) {
          case "Line":
            this.tool = new Line();
            break;
          case "Triangle":
            this.tool = new Triangle();
            break;
          case "Square":
            this.tool = new Square();
            break;
          case "Circle":
            this.tool = new Circle();
            break;
          case "Polygon":
            this.tool = new Polygon();
            break;

          default:
            throw new Error();
        }
      });
    });
  }
}
