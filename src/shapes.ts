export class Point {
  constructor(public x: number = 0, public y: number = 0) {}
}

export abstract class Shape {
  points: Array<Point> = [];

  abstract setPointAndDraw(
    p: Point,
    c: CanvasRenderingContext2D,
    vertex?: number
  ): string;

  reset(): void {
    this.points = [];
  }
}

export class Line extends Shape {
  vertex: number = 2;

  setPointAndDraw(point: Point, ctx: CanvasRenderingContext2D): string {
    this.points.push(point);

    // Draw If all vertices are specified
    if (this.points.length >= this.vertex) {
      ctx.beginPath();
      ctx.moveTo(this.points[0].x, this.points[0].y);
      ctx.lineTo(this.points[1].x, this.points[1].y);
      ctx.stroke();

      let length: number = Math.sqrt(
        (this.points[1].x - this.points[0].x) ** 2 +
          (this.points[1].y - this.points[0].y) ** 2
      );

      this.reset();

      return `Length: ${Math.floor(length)} px.`;
    }

    return "";
  }
}

export class Triangle extends Shape {
  vertex: number = 3;

  setPointAndDraw(point: Point, ctx: CanvasRenderingContext2D): string {
    this.points.push(point);

    if (this.points.length >= this.vertex) {
      ctx.beginPath();
      ctx.moveTo(this.points[0].x, this.points[0].y);
      ctx.lineTo(this.points[1].x, this.points[1].y);
      ctx.lineTo(this.points[2].x, this.points[2].y);
      ctx.lineTo(this.points[0].x, this.points[0].y);
      ctx.stroke();

      let sideA: number = Math.sqrt(
        (this.points[1].x - this.points[0].x) ** 2 +
          (this.points[1].y - this.points[0].y) ** 2
      );

      let sideB: number = Math.sqrt(
        (this.points[2].x - this.points[1].x) ** 2 +
          (this.points[2].y - this.points[1].y) ** 2
      );

      let sideC: number = Math.sqrt(
        (this.points[2].x - this.points[0].x) ** 2 +
          (this.points[2].y - this.points[0].y) ** 2
      );

      let semiPerimeter = (sideA + sideB + sideC) / 2;

      let area = Math.sqrt(
        semiPerimeter *
          (semiPerimeter - sideA) *
          (semiPerimeter - sideB) *
          (semiPerimeter - sideC)
      );

      this.reset();

      return `Area: ${Math.floor(area)} px<sup>2</sup>. Perimeter: ${Math.floor(
        sideA + sideB + sideC
      )} px.`;
    }

    return "";
  }
}

export class Square extends Shape {
  vertex: number = 2;

  setPointAndDraw(point: Point, ctx: CanvasRenderingContext2D): string {
    this.points.push(point);

    if (this.points.length >= this.vertex) {
      ctx.beginPath();
      ctx.moveTo(this.points[0].x, this.points[0].y);
      ctx.lineTo(this.points[0].x, this.points[1].y);
      ctx.lineTo(this.points[1].x, this.points[1].y);
      ctx.lineTo(this.points[1].x, this.points[0].y);
      ctx.lineTo(this.points[0].x, this.points[0].y);
      ctx.stroke();

      let sideA: number = Math.abs(this.points[1].x - this.points[0].x);
      let sideB: number = Math.abs(this.points[1].y - this.points[0].y);
      this.reset();

      return `Area: ${Math.floor(
        sideA * sideB
      )} px<sup>2</sup>. Perimeter: ${Math.floor(sideA * 2 + sideB * 2)} px.`;
    }

    return "";
  }
}

export class Circle extends Shape {
  vertex: number = 2;

  setPointAndDraw(point: Point, ctx: CanvasRenderingContext2D): string {
    this.points.push(point);

    if (this.points.length >= this.vertex) {
      ctx.beginPath();
      let x = this.points[0].x;
      let y = this.points[0].y;

      let radius = Math.sqrt(
        (this.points[1].x - x) ** 2 + (this.points[1].y - y) ** 2
      );

      ctx.arc(x, y, radius, 0, Math.PI * 2, true);
      ctx.stroke();
      this.reset();

      return `Area: ${Math.floor(
        Math.PI * radius ** 2
      )} px<sup>2</sup>. Circumference: ${Math.floor(
        2 * Math.PI * radius
      )} px.`;
    }

    return "";
  }
}

export class Polygon extends Shape {
  vertex: number;

  setPointAndDraw(
    point: Point,
    ctx: CanvasRenderingContext2D,
    vtx: number = 3
  ): string {
    this.points.push(point);
    this.vertex = vtx;

    if (this.points.length >= this.vertex) {
      ctx.beginPath();

      this.points.forEach((point, index) => {
        if (index === 0) {
          ctx.moveTo(point.x, point.y);
        } else {
          ctx.lineTo(point.x, point.y);
        }
      });
      ctx.lineTo(this.points[0].x, this.points[0].y);
      ctx.stroke();

      let sideA: number = 0;
      let sideB: number = 0;
      let perimeter: number = 0;

      // Gauss's area formula + perimeter
      this.points.forEach((_, i, arr) => {
        if (i + 1 < arr.length) {
          sideA += arr[i].x * arr[i + 1].y;
          sideB += arr[i].y * arr[i + 1].x;
          perimeter += Math.sqrt(
            (arr[i + 1].x - arr[i].x) ** 2 + (arr[i + 1].y - arr[i].y) ** 2
          );
        } else {
          sideA += arr[i].x * arr[0].y;
          sideB += arr[i].y * arr[0].x;
          perimeter += Math.sqrt(
            (arr[0].x - arr[i].x) ** 2 + (arr[0].y - arr[i].y) ** 2
          );
        }
      });

      let area: number = Math.abs(sideA - sideB) / 2;

      this.reset();

      return `Area: ${Math.floor(area)} px<sup>2</sup>. Perimeter: ${Math.floor(
        perimeter
      )} px.`;
    }

    return "";
  }
}
